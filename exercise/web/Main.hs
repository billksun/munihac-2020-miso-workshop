{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Data.Aeson (FromJSON, ToJSON)
import Data.Foldable (asum, find)
import Data.List (transpose, (!!))
import Data.Map ()
import Data.Maybe (isJust, isNothing)
import Data.Proxy
import GHC.Generics
import Lens.Micro (ix, (&), (?~), (^.))
import Miso
import Miso.Effect.Storage
import Miso.String (MisoString, toMisoString)
import Servant.API
import Servant.Client.Ghcjs
import Servant.Client.Internal.XhrClient

main :: IO ()
main = do
  currentUri <- getCurrentURI
  startApp App {model = initModel currentUri, ..}
  where
    initialAction = Initialize
    update = updateModel
    view = viewModel
    events = defaultEvents
    subs = [uriSub ChangeURI]
    mountPoint = Nothing
    logLevel = Off

data Square
  = X
  | O
  deriving (Show, Eq)

type Grid = [[Maybe Square]]

emptyGrid, aGrid :: Grid
emptyGrid = replicate 3 (replicate 3 Nothing)
aGrid =
  [ [Just X, Nothing, Nothing],
    [Nothing, Just O, Nothing],
    replicate 3 Nothing
  ]

hasWinner :: Grid -> Maybe Square
hasWinner g =
  asum (map isWinnerRow thingToCheck)
  where
    thingToCheck =
      g ++ transpose g
        ++ [ [g !! 0 !! 0, g !! 1 !! 1, g !! 2 !! 2],
             [g !! 0 !! 2, g !! 1 !! 1, g !! 2 !! 0]
           ]
    isWinnerRow :: [Maybe Square] -> Maybe Square
    isWinnerRow row
      | all isJust row,
        all (== head row) row =
        head row
      | otherwise =
        Nothing

data Model = Model
  { currentUri :: URI,
    grid :: Grid,
    currentPlayer :: MisoString,
    winnerName :: MisoString,
    winnerMsg :: MisoString,
    isNewGameButtonDisabled :: Bool,
    player1Name :: MisoString,
    player1Country :: Maybe MisoString,
    player2Name :: MisoString,
    player2Country :: Maybe MisoString,
    totalMoves :: Int,
    stats :: [Stat],
    countries :: [Country],
    loadStatus :: LoadStatus
  }
  deriving (Show, Eq)

initModel :: URI -> Model
initModel uri =
  Model
    { currentUri = uri,
      grid = emptyGrid,
      currentPlayer = "Player 1",
      winnerName = "",
      winnerMsg = "",
      isNewGameButtonDisabled = True,
      player1Name = "",
      player1Country = Nothing,
      player2Name = "",
      player2Country = Nothing,
      totalMoves = 0,
      stats = [],
      countries = [],
      loadStatus = Loading
    }

data Stat = Stat
  { statPlayer1Name :: MisoString,
    statPlayer2Name :: MisoString,
    statWinnerName :: MisoString,
    numberOfMoves :: Int
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data Country = Country
  { name :: MisoString,
    flag :: MisoString
  }
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data LoadStatus
  = Loading
  | Loaded
  | Error
  deriving (Show, Eq, Generic, FromJSON, ToJSON)

data Action
  = None
  | Initialize
  | LoadCountriesFinished [Country]
  | LoadStats
  | LoadStatsFinished [Stat]
  | ClickSquare Int Int
  | StartNewGame
  | ChangePlayer1Name MisoString
  | ChangePlayer1Country MisoString
  | ChangePlayer2Name MisoString
  | ChangePlayer2Country MisoString
  | UpdateLocalStorage
  | LoadError
  | ChangeURI {newURI :: URI}
  deriving (Show, Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel None m = noEff m
updateModel Initialize m =
  m <# do
    response <- runClientMOrigin getFlags clientEnv
    case response of
      Left _ -> pure LoadError
      Right countries -> pure $ LoadCountriesFinished countries
updateModel LoadError m = noEff $ m {loadStatus = Error}
updateModel (LoadCountriesFinished countries) m =
  m {countries = countries} <# do
    pure LoadStats
updateModel LoadStats m =
  m <# do
    store <- getLocalStorage localStorageKey
    case store of
      Left _ -> pure $ LoadStatsFinished []
      Right stats -> pure $ LoadStatsFinished stats
updateModel (LoadStatsFinished stats) m = noEff (m {stats = stats})
updateModel StartNewGame m = noEff ((initModel (currentUri m)) {stats = stats m})
updateModel (ChangePlayer1Name newName) m = noEff (m {player1Name = newName})
updateModel (ChangePlayer1Country newCountry) m = noEff (m {player1Country = Just newCountry})
updateModel (ChangePlayer2Name newName) m = noEff (m {player2Name = newName})
updateModel (ChangePlayer2Country newCountry) m = noEff (m {player2Country = Just newCountry})
updateModel (ClickSquare rowId colId) m
  | isJust (grid m !! rowId !! colId) = noEff m
  | hasGameEnded m = noEff m
  | currentPlayer m == "Player 1" = updateNewGameButtonState (updateCurrentPlayer "Player 2" $ updateWinnerMsg $ incrementMoves $ updateGrid rowId colId X m) <# pure UpdateLocalStorage
  | currentPlayer m == "Player 2" = updateNewGameButtonState (updateCurrentPlayer "Player 1" $ updateWinnerMsg $ incrementMoves $ updateGrid rowId colId O m) <# pure UpdateLocalStorage
  | otherwise = noEff m
  where
    updateNewGameButtonState m
      | hasGameEnded m = m {isNewGameButtonDisabled = False}
      | otherwise = m {isNewGameButtonDisabled = True}
    hasGameEnded m
      | isJust (hasWinner (grid m)) = True
      | isFull (grid m) = True
      | otherwise = False
      where
        isFull = all isJust . concat
    updateCurrentPlayer value m = m {currentPlayer = value}
    updateGrid rowId colId value m = m {grid = grid m & ix rowId . ix colId ?~ value}
    updateWinnerMsg m@Model {..}
      | isJust (hasWinner grid)
          && currentPlayer == "Player 1" =
        recordStats m {winnerMsg = player1Name <> " wins!", winnerName = player1Name}
      | isJust (hasWinner grid)
          && currentPlayer == "Player 2" =
        recordStats m {winnerMsg = player2Name <> " wins!", winnerName = player2Name}
      | hasGameEnded m = m {winnerMsg = "Draw !"}
      | otherwise = m {winnerMsg = ""}
    incrementMoves m@Model {..} = m {totalMoves = totalMoves + 1}
    recordStats m@Model {..} = m {stats = new}
      where
        new = [Stat {statPlayer1Name = player1Name, statPlayer2Name = player2Name, statWinnerName = winnerName, numberOfMoves = totalMoves}] <> stats
updateModel (ChangeURI newUri) m = noEff $ m {currentUri = newUri}
updateModel UpdateLocalStorage m = m <# updateLocalStorage (stats m)

bootstrapUrl :: MisoString
bootstrapUrl = "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"

viewModel :: Model -> View Action
viewModel m =
  div_
    [class_ "container"]
    [ headerView,
      newGameView m,
      if uriFragment (currentUri m) == "#stats"
        then statsView m
        else contentView m,
      link_
        [ rel_ "stylesheet",
          href_ bootstrapUrl
        ]
    ]

headerView :: View Action
headerView =
  nav_
    [class_ "navbar navbar-dark bg-dark"]
    [ h2_
        [class_ "bd-title text-light row"]
        [ a_
            [class_ "nav-link text-light", href_ "#"]
            [text "Tic-Tac-Toe"],
          span_
            [class_ "badge badge-warning"]
            [text "in miso!"],
          a_
            [class_ "nav-link text-light", href_ "#stats"]
            [text "Stats"]
        ]
    ]

newGameView :: Model -> View Action
newGameView Model {..} =
  nav_
    [class_ "navbar navbar-light bg-light"]
    [ form_
        [class_ "form-inline"]
        [ input_
            [ class_ "form-control mr-sm-2",
              type_ "text",
              value_ player1Name,
              onChange ChangePlayer1Name,
              placeholder_ "Player 1"
            ],
          select_
            [ class_ "custom-select",
              style_ [("margin-right", "15px")],
              onChange ChangePlayer1Country
            ]
            ( flip map countries $ \option ->
                option_ [] [text (name option)]
            ),
          input_
            [ class_ "form-control mr-sm-2",
              type_ "text",
              value_ player2Name,
              onChange ChangePlayer2Name,
              placeholder_ "Player 2"
            ],
          select_
            [ class_ "custom-select",
              style_ [("margin-right", "15px")],
              onChange ChangePlayer2Country
            ]
            ( flip map countries $ \option ->
                option_ [] [text (name option)]
            ),
          button_
            [ class_ "btn btn-outline-warning",
              type_ "button",
              onClick StartNewGame,
              disabled_ isNewGameButtonDisabled
            ]
            [text "New game"]
        ]
    ]

contentView :: Model -> View Action
contentView m@Model {..} =
  div_
    [style_ [("margin", "20px")]]
    [ gridView m,
      alertView winnerMsg
    ]

gridView :: Model -> View Action
gridView Model {..} =
  div_
    [style_ [("margin", "20px")]]
    [ div_
        [class_ "row justify-content-around align-items-center"]
        [ div_
            [class_ "row justify-content-right align-items-center"]
            [ flagView countries player1Country,
              h3_
                [ class_
                    ( if currentPlayer == "Player 1"
                        then "text-success"
                        else "text-body"
                    )
                ]
                [text player1Name]
            ],
          div_
            [style_ [("display", "inline-block")]]
            [ div_
                [ style_
                    [ ("display", "grid"),
                      ("grid-template-rows", "1fr 1fr 1fr"),
                      ("grid-template-columns", "1fr 1fr 1fr"),
                      ("grid-gap", "2px")
                    ]
                ]
                ( flip concatMap (zip [0 ..] grid) $ \(rowId, row) ->
                    flip map (zip [0 ..] row) $ \(colId, sq) ->
                      cell rowId colId sq
                )
            ],
          div_
            [class_ "row justify-content-left align-items-center"]
            [ h3_
                [ class_
                    ( if currentPlayer == "Player 2"
                        then "text-success"
                        else "text-body"
                    )
                ]
                [text player2Name],
              flagView countries player2Country
            ]
        ]
    ]
  where
    cell :: Int -> Int -> Maybe Square -> View Action
    cell rowId colId square =
      div_
        [style_ [("width", "100px"), ("height", "100px")]]
        [ button_
            [ type_ "button",
              style_
                [ ("width", "100%"),
                  ("height", "100%"),
                  ("font-size", "xxx-large")
                ],
              class_ "btn btn-outline-secondary",
              onClick (ClickSquare rowId colId)
            ]
            [text (showCellContent $ grid !! rowId !! colId)]
        ]

showCellContent :: Maybe Square -> MisoString
showCellContent (Just X) = "X"
showCellContent (Just O) = "O"
showCellContent Nothing = ""

alertView :: MisoString -> View Action
alertView v =
  div_
    [ class_ "alert alert-warning",
      style_ [("text-align", "center")]
    ]
    [ h4_
        [class_ "alert-heading"]
        [text v]
    ]

toTextStats :: [Stat] -> [MisoString]
toTextStats = map (\Stat {..} -> statPlayer1Name <> " vs. " <> statPlayer2Name <> ", won by " <> statWinnerName <> " in " <> toMisoString numberOfMoves <> " moves")

statsView :: Model -> View Action
statsView m =
  div_
    [ class_ "row justify-content-around align-items-center",
      style_ [("margin-bottom", "20px")]
    ]
    [ ul_
        [class_ "list-group"]
        ( flip map (toTextStats $ stats m) $ \elt ->
            ul_ [class_ "list-group-item"] [text elt]
        )
    ]

flagView :: [Country] -> Maybe MisoString -> View Action
flagView countries (Just selectedCountry)
  | Just c <- find ((== selectedCountry) . name) countries =
    div_
      [style_ [("margin", "20px")]]
      [ div_
          []
          [img_ [src_ (flag c), width_ "50"]]
      ]
flagView _ _ = div_ [] []

localStorageKey :: MisoString
localStorageKey = "game-history"

updateLocalStorage :: [Stat] -> JSM Action
updateLocalStorage new = do
  setLocalStorage localStorageKey new
  pure None

type API = "all" :> Get '[JSON] [Country]

getFlags :: ClientM [Country]
getFlags = client (Proxy @API)

clientEnv :: ClientEnv
clientEnv = ClientEnv (BaseUrl Https "restcountries.eu" 443 "rest/v2")
